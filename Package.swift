// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "CSVCoder",
    products: [
        .library(name: "CSVCoder", targets: ["CSVCoder"]),
    ],
    targets: [
        .target(name: "CSVCoder"),
        .testTarget(name: "CSVCoderTests", dependencies: ["CSVCoder"]),
    ]
)
