
import Foundation

public enum LineDelimiter {
    case newline
    case custom(String)
}

extension String {

    func components(separatedBy delimiter: LineDelimiter) -> [String] {
        return components(separatedBy: delimiter.string)
    }
}

extension LineDelimiter {

    fileprivate var string: String {
        switch self {
        case .newline: return "\n"
        case .custom(let string): return string
        }
    }
}
