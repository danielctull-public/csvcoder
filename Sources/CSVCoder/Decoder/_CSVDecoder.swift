
import Foundation

final class _CSVDecoder: Decoder {

    struct NoRecordError: Error {}

    private let header: Header
    private let records: [Record]
    init(header: Header, records: [Record]) {
        self.header = header
        self.records = records
    }

    var codingPath: [CodingKey] = []

    var userInfo: [CodingUserInfoKey: Any] = [:]

    func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key: CodingKey {
        guard let record = records.first else { throw NoRecordError() }
        let container = KeyedContainer<Key>(header: header, record: record, codingPath: codingPath)
        return KeyedDecodingContainer(container)
    }

    func unkeyedContainer() throws -> UnkeyedDecodingContainer {
        return UnkeyedContainer(codingPath: codingPath, header: header, records: records)
    }

    func singleValueContainer() throws -> SingleValueDecodingContainer {
        fatalError()
    }
}
