//
//  File.swift
//  
//
//  Created by Daniel Tull on 29/07/2019.
//

import Foundation

extension _CSVDecoder {

    struct UnkeyedContainer {
        let codingPath: [CodingKey]
        var currentIndex = 0
        private let header: Header
        private let records: [Record]

        init(codingPath: [CodingKey], header: Header, records: [Record]) {
            self.codingPath = codingPath
            self.header = header
            self.records = records
        }
    }
}

extension _CSVDecoder.UnkeyedContainer: UnkeyedDecodingContainer {

    var count: Int? { return records.count }
    var isAtEnd: Bool { return currentIndex >= records.count }

    mutating func decodeNil() throws -> Bool {
        return false
    }

    mutating func decode<T>(_ type: T.Type) throws -> T where T : Decodable {

        guard !isAtEnd else {
            throw DecodingError.dataCorruptedError(in: self, debugDescription: "Is at end")
        }

        defer { currentIndex += 1 }

        let record = records[currentIndex]
        let decoder = _CSVDecoder(header: header, records: [record])
        return try T(from: decoder)
    }

    mutating func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
        throw DecodingError.dataCorruptedError(in: self, debugDescription: "Nested Containers are unsupported.")
    }

    mutating func nestedUnkeyedContainer() throws -> UnkeyedDecodingContainer {
        throw DecodingError.dataCorruptedError(in: self, debugDescription: "Nested Containers are unsupported.")
    }

    mutating func superDecoder() throws -> Decoder {
        throw DecodingError.dataCorruptedError(in: self, debugDescription: "Nested Containers are unsupported.")
    }
}
