
import Foundation

extension _CSVDecoder {

    struct KeyedContainer<Key: CodingKey> {

        let header: Header
        let record: Record
        let codingPath: [CodingKey]
        init(header: Header, record: Record, codingPath: [CodingKey]) {
            self.header = header
            self.record = record
            self.codingPath = codingPath
        }
    }
}

extension _CSVDecoder.KeyedContainer: KeyedDecodingContainerProtocol {

    private var context: DecodingError.Context {
        return DecodingError.Context(codingPath: codingPath, debugDescription: "Header: \(header) Record: \(record)")
    }

    var allKeys: [Key] { return header.compactMap { Key(stringValue: $0.string) } }

    func contains(_ key: Key) -> Bool {
        return allKeys.contains(where: { $0.stringValue == key.stringValue })
    }

    func decodeNil(forKey key: Key) throws -> Bool {
        let string = try decode(String.self, forKey: key)
        return string.isEmpty
    }

    func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool {
        return false
    }

    func decode(_ type: String.Type, forKey key: Key) throws -> String {

        guard
            let headerValue = header.first(where: { $0.string == key.stringValue }),
            headerValue.index < record.count
        else {
            throw DecodingError.keyNotFound(key, context)
        }

        return record[headerValue.index].string
    }

    func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T: Decodable, T: LosslessStringConvertible {
        let string = try decode(String.self, forKey: key)
        guard let value = T(string) else {
            throw DecodingError.typeMismatch(type, context)
        }
        return value
    }

    func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T: Decodable {
        let decoder = _CSVDecoder(header: header, records: [record])
        return try T(from: decoder)
    }

    func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
        throw DecodingError.dataCorruptedError(forKey: key, in: self, debugDescription: "Nested Containers are unsupported.")
    }

    func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer {
        throw DecodingError.dataCorruptedError(forKey: key, in: self, debugDescription: "Nested Containers are unsupported.")
    }

    func superDecoder() throws -> Decoder {
        throw DecodingError.dataCorrupted(context)
    }

    func superDecoder(forKey key: Key) throws -> Decoder {
        throw DecodingError.dataCorruptedError(forKey: key, in: self, debugDescription: "Nested Containers are unsupported.")
    }
}
