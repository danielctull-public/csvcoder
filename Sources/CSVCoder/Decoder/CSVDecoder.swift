
import Foundation

public final class CSVDecoder {

    struct DataDecodingError: Error {}
    struct NoContentError: Error {}

    /// The strategy to use in decoding dates. Defaults to `.deferredToDate`.
    public var dateDecodingStrategy: DateDecodingStrategy = .deferredToDate

    /// The strategy to use in decoding binary data. Defaults to `.base64`.
    public var dataDecodingStrategy: DataDecodingStrategy = .base64

    public var lineDelimiter: LineDelimiter = .newline

    public var separator: Separator = .comma

    /// Initializes `self` with default strategies.
    public init() {}

    /// Decodes a top-level value of the given type from the given JSON representation.
    ///
    /// - parameter type: The type of the value to decode.
    /// - parameter data: The data to decode from.
    /// - returns: A value of the requested type.
    /// - throws: `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid JSON.
    /// - throws: An error if any value throws an error during decoding.
    public func decode<T : Decodable>(_ type: T.Type, from data: Data) throws -> T {

        guard let string = String(data: data, encoding: .utf8) else {
            throw DataDecodingError()
        }

        var lines = string.components(separatedBy: lineDelimiter)

        guard !lines.isEmpty else { throw NoContentError() }

        let header = Header(lines.removeFirst().components(separatedBy: separator))
        let records = lines.map { Record($0.components(separatedBy: separator)) }

        let decoder = _CSVDecoder(header: header, records: records)
        return try T(from: decoder)
    }
}
