
import Foundation

public enum Separator {
    case comma
    case custom(String)
}

extension String {

    func components(separatedBy separator: Separator) -> [String] {
        return components(separatedBy: separator.string)
    }
}

extension Separator {

    fileprivate var string: String {
        switch self {
        case .comma: return ","
        case .custom(let string): return string
        }
    }
}
