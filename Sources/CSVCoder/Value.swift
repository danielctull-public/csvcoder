
struct Value {
    let index: Int
    let string: String
}

typealias Record = [Value]
typealias Header = [Value]


extension Array where Element == Value {

    init(_ strings: [String]) {
        self = strings.enumerated().map(Value.init)
    }
}
