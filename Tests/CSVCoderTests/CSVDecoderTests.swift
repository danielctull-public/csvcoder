import XCTest
@testable import CSVCoder

final class CSVCoderTests: XCTestCase {

    struct Event: Decodable {
        let name: String
    }

    func testEvents() throws {

        let data = """
        name
        Party
        Birthday!
        """.data(using: .utf8)!

        let decoder = CSVDecoder()
        let events = try decoder.decode([Event].self, from: data)
        XCTAssertEqual(events.first?.name, "Party")
        XCTAssertEqual(events.last?.name, "Birthday!")
    }
}
