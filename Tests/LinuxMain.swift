import XCTest

import CSVCoderTests

var tests = [XCTestCaseEntry]()
tests += CSVCoderTests.__allTests()

XCTMain(tests)
